## Exam - API Test Automation

This API test automation is written in Python using pytest framework.
---

## About the tests

Application URL: https://jsonplaceholder.typicode.com/users  

The following tests are automated:  
**TC1: Verify GET Users request**  
1. Verify 200 OK message is returned  
2. Verify that there are 10 users in the results  
**TC2: Verify GET User request by Id**  
1. Verify 200 OK message is returned  
2. Verify if user with id8 is Nicholas Runolfsdottir V  
**TC3: Verify POST Users request**  
1. Verify 201 Created message is returned  
2. Verify that the posted data are showing up in the result  

## Set up (Windows)
1. Download and install Python 3 (https://www.python.org/downloads/)  
2. Install libraries:  
pip install -U requests  
pip install -U pytest pytest-html  
pip install -U jsonschema  

## How to run the test locally
On your test directory (e.g. C:\Exam), open the Windows Command Prompt and type in:  
pytest Datacom_API_tests.py